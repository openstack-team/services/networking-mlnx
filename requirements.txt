# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.

Babel>=1.3
pbr!=2.1.0,>=2.0.0 # Apache-2.0
defusedxml>=0.5.0 # PSF

eventlet!=0.18.3,>=0.18.2 # MIT
netaddr>=0.7.18 # BSD
pyroute2>=0.5.7
python-neutronclient>=5.1.0 # Apache-2.0
SQLAlchemy!=1.1.5,!=1.1.6,!=1.1.7,!=1.1.8,>=1.0.10 # MIT
six>=1.10.0 # MIT
oslo.config>=5.2.0 # Apache-2.0
oslo.concurrency>=3.26.0 # Apache-2.0
oslo.privsep>=1.32.0 # Apache-2.0
python-openstackclient>=3.3.0 # Apache-2.0
neutron-lib>=1.28.0 # Apache-2.0
pyzmq

# Using neutron master is necessary for neutron-db-manage test,
# Ussuri DB branch was created in master[1] and not yet released.
# [1] https://github.com/openstack/neutron/commit/da93f09676a6531394bf898e7292c2ae87194ce3
-e git+https://git.openstack.org/openstack/neutron#egg=neutron
